\chapter{Local methods}

Local methods like Sum of Absolute Differences - SAD \cite{SAD}, Normalized Cross Correlation - NCC \cite{NCC} or Sum of Squared Differences - 
SSD \cite{SSD_1} which are presented by formulas \ref{eq:SAD}, \ref{eq:NCC} and \ref{eq:SSD} respectively, trying to minimize or 
maximize local energy between two pixels in each image to find a position of pixel from one image in the second one. They use window centered on 
a pixel to calculate local scores of similarity for pixels in one image and second one by translating one image by the one axis for each disparity 
in the range. This is similar to punting and translating one a bit transparent image on another one and trying to find where each pixel appears in 
both images.

\begin{equation}
  \underset{ (i,j) \in W } { \Sigma } |I_1(i,j) - I_2(x+i,y+j)|
  \label{eq:SAD}
\end{equation}

\begin{equation}
  \underset{ (i,j) \in W } { \Sigma } (I_1(i,j) - I_2(x+i,y+j))^2
  \label{eq:SSD}
\end{equation}

\begin{equation}
  \frac{ \underset{ (i,j) \in W_p } { \Sigma } (I_1(i,j) - \overset{-}{I_1}(p_i,p_j))(I_2(i+d,j) - \overset{-}{I_2}(p_i+d,p_j))}
  { \sqrt{ \underset{ (i,j) \in W_p} { \Sigma } (I_1(i,j) - \overset{-}{I_1}(p_i,p_j))^2  \underset{ (i,j) \in W_p} { \Sigma } (I_2(i+d,j) - 
\overset{-}{I_2}(p_i+d,p_j))^2} }
 \label{eq:NCC}
\end{equation}

These methods in many cases can be implemented in a very efficient way but by taking only local information into account they are not robust when 
e.g. one image is affected by noise or there is a difference between light intensity in the both images. Figures \ref{fig:Tsukuba}, \ref{fig:Cones}, 
\ref{fig:Art} present disparity maps calculated by SAD, NCC and SSD for images "Tsukuba", "Cones" and "Art" from the Middlebury database used at 
first in \cite{FirstDataSet}, \cite{FirstDataSet_2} and \cite{FirstDataSet_3}. These images was obtained for different ranges of disparities, each 
which in author opinion give the best result.

\begin{figure}[htp]
  \centering 
  \mbox{
      \subfloat[\label{fig:TsukubaLeft}]{\includegraphics[scale=0.3]{thsukuba_left.eps}}\quad
      \subfloat[\label{fig:TsukubaRight}]{\includegraphics[scale=0.3]{thsukuba_right.eps}}\quad
  }
  \mbox{
      \subfloat[\label{fig:TsukubaSAD}]{\includegraphics[scale=0.3]{thsukuba_sad_3.eps}}\quad
      \subfloat[\label{fig:TsukubaSSD}]{\includegraphics[scale=0.3]{thsukuba_ssd_3.eps}}\quad
  }
  \mbox{
      \subfloat[\label{fig:TsukubaNCC}]{\includegraphics[scale=0.3]{thsukuba_ncc_3.eps}}\quad
      \subfloat[\label{fig:TsukubaGrTruth}]{\includegraphics[scale=0.35]{thsukuba_ground_truth.eps}}\quad
  }
  \caption{Tsukuba - \ref{fig:TsukubaLeft} and \ref{fig:TsukubaRight} left and right image. \ref{fig:TsukubaSAD}, \ref{fig:TsukubaSSD}, 
\ref{fig:TsukubaNCC} 
and - SAD, SSD, NCC  respectively, calculated for range of disparities 0-30 and window size 3. \ref{fig:TsukubaGrTruth} a ground truth. }
  \label{fig:Tsukuba}
\end{figure}

\begin{figure}[htp]
  \centering 
  \mbox{
      \subfloat[\label{fig:ConesLeft}]{\includegraphics[scale=0.3]{cones_left.eps}}\quad
      \subfloat[\label{fig:ConesRight}]{\includegraphics[scale=0.3]{cones_right.eps}}\quad
  }
  \mbox{
      \subfloat[\label{fig:ConesSAD}]{\includegraphics[scale=0.3]{cones_sad_3.eps}}\quad
      \subfloat[\label{fig:ConesSSD}]{\includegraphics[scale=0.3]{cones_ssd_3.eps}}\quad
  }
  \mbox{
      \subfloat[\label{fig:ConesNCC}]{\includegraphics[scale=0.3]{cones_ncc_3.eps}}\quad
      \subfloat[\label{fig:ConesGrTruth}]{\includegraphics[scale=0.25]{cones_ground_truth.eps}}\quad
  }
  \caption{Cones - \ref{fig:ConesLeft} and \ref{fig:ConesRight} left and right image. \ref{fig:ConesSAD}, \ref{fig:ConesSSD} and \ref{fig:ConesNCC} - 
SAD, SSD, NCC,respectively calculated for range of disparities 10-50 and window size 3. \ref{fig:ConesGrTruth} ground truth.}
  \label{fig:Cones}
\end{figure}

\begin{figure}[htp]
  \centering 
  \mbox{
      \subfloat[\label{fig:ArtLeft}]{\includegraphics[scale=0.3]{art_left}}\quad
      \subfloat[\label{fig:ArtRight}]{\includegraphics[scale=0.3]{art_right}}\quad
  }
  \mbox{
      \subfloat[\label{fig:ArtSAD}]{\includegraphics[scale=0.3]{art_sad_3.eps}}\quad
      \subfloat[\label{fig:ArtSSD}]{\includegraphics[scale=0.3]{art_ssd_3.eps}}\quad
  }
  \mbox{
      \subfloat[\label{fig:ArtNCC}]{\includegraphics[scale=0.3]{art_ncc_3.eps}}\quad
      \subfloat[\label{fig:ArtGrTruth}]{\includegraphics[scale=0.1]{art_ground_truth}}\quad
  }
  \caption{Art - \ref{fig:ArtLeft} and \ref{fig:ArtRight} left and right image. \ref{fig:ArtSAD}, \ref{fig:ArtSSD} and \ref{fig:ArtNCC} - SAD, SSD 
and NCC, respectively calculated for range of disparities 0-30 and window size 3. \ref{fig:ArtGrTruth} ground truth.}
  \label{fig:Art}
\end{figure}

\break
\clearpage
\section{Implementation}

These methods implemented directly from definitions are very slow, especially NCC. Also as we will see in the next chapter we will use scores from 
local methods as input for the graph cut method so we will wish to calculate these scores fast as possible for a wide range of disparities. Because 
author used Python at first we will wish to avoid as many loops as possible. In case of SAD or SSD optimization is straightforward and pseudocode is 
presented on listening \ref{ListSAD}. Implementation in the  Python can be find in the source code added to this report.  

\begin{lstlisting}[label=ListSAD,caption=Optimized version of SAD]
for disparity in range_of_disparities
1.    # compute the score at that disparity for all the pixel
2.    translate the entire right image
3.    measure the absolute difference for each pixel 
      between the left image and the translated right image
4.    convolve that image of absolute differences by a 
      rectangle (sliding average) or a gaussian
5.    update the best disparity map
\end{lstlisting}
\break
In the case of NCC optimization is not so obvious but author used method which based on the one proposed in 
\cite{shen2011Effnorcrocorcalmetstevisbasrobnav}. This modified version is using convolution instead of 
Integral image framework \cite{SAT}.

\subsection{Definition of NCC with convolution - \textbf{below text was originally written by Martin de La Gorce}}

$w(i,j)$ - smoothing kernel with finite support  such that  
$$\sum_{i}\sum_jw(i,j)=1$$
$A$ and $B$ the two images we want to use for the stereo reconstruction.

We define $\bar{A}$ as 
$$\bar{A}=w\otimes A$$
with $\otimes$ the convolution 
i.e 
$$\bar{A}(x,y)=\sum_i\sum_jA(x+i,y+1)w(i,j)$$
and 
$$\bar{B}=w\otimes B$$
We denote $B_d$ defined by 
$B_d(x,y)=B(x+d,y)$ and $\bar{B_d}(x,y)=\bar{B}(x+d,y)$
We want to compute the normalized cross correlation scores for a set of disparities
$d$
$$C_{d}(x,y)=\frac{N_{d}(x,y)}{D_A(x,y)+D_{Bd}(x,y)}$$
with $$N_{d}(x,y)=\sum_{i}\sum_jw(i,j)(A(x+i,y+j)-\bar{A}(x,y))(B_{d}(x+i,y+j)-\bar{B}(x,y))$$
and $$D_A(x,y)=\sqrt{\sum_{i}\sum_jw(i,j)(A(x+i,y+j)-\bar{A}(x,y))^2}$$
and $$D_{Bd}(x,y,d)=\sqrt{\sum_{i}\sum_jw(i,j)(B_{d}(x+i,y+j)-\bar{B}(x,y))^2}$$

$$$$

\begin{eqnarray}
N_{d}(x,y)&=&\sum_{i}\sum_jw(i,j)(A(x+i,y+j)-\bar{A}(x,y))(B_{d}(x+i,y+j)-\bar{B_{d}}(x,y))\nonumber\\
&=&\sum_{i}\sum_jw(i,j)A(x+i,y+j)B_{d}(x+i,y+j)\nonumber\\
&&-\bar{B_{d}}(x,y)\sum_{i}\sum_jw(i,j)A(x+i,y+j))\nonumber\\
&&-\bar{A}(x,y)\sum_{i}\sum_jw(i,j)B_{d}(x+i,y+j))\nonumber\\
&&+\bar{A}(x,y)\bar{B}(x,y)\sum_{i}\sum_jw(i,j)
\end{eqnarray}

$$N_{d}=w\otimes(AB_d)-\bar{A}\bar{B_d}$$
Similarly we can show that
$$D_A=\sqrt{w\otimes(A^{2})-\bar{A}^2}$$
$$D_{Bd}=\sqrt{w\otimes(B_d^{2})-\bar{B_d}^2}$$
